import React from "react";
import ReactDOM from "react-dom/client";

import {BrowserRouter, Route, Routes} from "react-router-dom";
import Navigation from "./components/Navigation";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/*Browser Router est un composant permettant la navigation en fonction d'url */}
    <BrowserRouter>
    {/*On appelle le composant navigation */}
      <Navigation />
      {/*On lui explicite les routes, Routes et route foncitonnent comme <ul/> et <li/> */}
      <Routes>
        {/*On lui donne le chemin url ainsi que le composant à afficher quand on clique dessus */}
        <Route path="/" element={<Home/>} />
        <Route path="/products" element={<ProductList/>} />
        <Route path="/cart" element={<Cart/>} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

