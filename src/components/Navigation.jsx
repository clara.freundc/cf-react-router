import { BrowserRouter, Route, Link, } from "react-router-dom";
import "./Navigation.css";
import CartItem from "../pages/cart/CartItem";
import ProductList from "../pages/products/ProductList";

function Navigation() {
  return (
      <nav>
        <ul className="Navigation">
          {/*Affiche les liens cliquables et leur apparence */}
          <li><Link to="/">Home</Link></li>
          <li><Link to="/products">Products</Link></li>
          <li><Link to="/cart">Cart</Link></li>
        </ul>
      </nav>
  );
}

export default Navigation;
